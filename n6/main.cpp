/*
    Дан файл. Зашифровать байтовым ключом 
    по модулю два. Записать ключ в первый 
    байт зашифрованного файла, затем 
    расшифровать файл.
*/

#include <cstdio>
#include <cstdlib>
#include <cstring>

void xor_crypto(FILE* f_in, FILE* f_out, char* key)
{
    int key_count = 0;
    int encrypt_byte;

    while((encrypt_byte = fgetc(f_in)) != EOF)
    {
        fputc(encrypt_byte ^ key[key_count], f_out);

        key_count++;
        if(key_count == strlen(key))
            key_count = 0;
    }
}

void encrypt_data(FILE* f_in, FILE* f_out, char* key)
{
    fputc((char)strlen(key), f_out);
    for(int i = 0; key[i] != '\0'; ++i)
        fputc(key[i], f_out);

    xor_crypto(f_in, f_out, key);
}

void decrypt_data(FILE* f_in, FILE* f_out)
{
    char key_l;
    char* key;

    fscanf(f_in, "%c", &key_l);
    key = new char[key_l];
    for(int i = 0; i < key_l; ++i)
        fscanf(f_in, "%c", &key[i]);

    xor_crypto(f_in, f_out, key);
}

int main()
{
    const char  f_in_name[]     = "file.in",
                f_out_name[]    = "file.out",
                f_t_name[]      = "file.t";
    char        xormask[]       = "3847611839";

    FILE *f_in, *f_out, *f_t;

    f_in = fopen(f_in_name, "r");
    if(f_in == NULL)
    {
        printf("Cannot open: %s\n", f_in_name);
        return -1;
    }

    f_out = fopen(f_out_name, "w");
    if(f_out == NULL)
    {
        fclose(f_in);
        printf("unable to open '%s' for writing.\n", f_in_name);
        return -1;
    }

    f_t = fopen(f_t_name, "w");
    if(f_t == NULL)
    {
        fclose(f_t);
        printf("unable to open '%s' for writing.\n", f_t_name);
        return -1;
    }

    encrypt_data(f_in, f_out, xormask);
    fclose(f_out);

    f_out = fopen(f_out_name, "r");
    decrypt_data(f_out, f_t);

    fclose(f_in);
    fclose(f_t);

    return 0;
}
