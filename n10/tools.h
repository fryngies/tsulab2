#ifndef tools_h
#define tools_h

inline int intEq(char ch)
{
    return ch - 48;
}

inline char charEq(int n)
{
    return n + 48;
}

inline char letterEq(int n)
{
    return n + 65;
}

inline const char endl()
{
    return '\n';
}

inline const char ends()
{
    return '\0';
}

char *strAppend(char *str, char ch, int n);

#endif /* tools_h */
