#ifndef rpn_h
#define rpn_h

int getWeight(char ch);
char *infix2postfix(char *infix, char *postfix, int size);
char *getVars(char *str, char *sym);
int calcPostfix(char *str, char *sym);

#endif /* rpn_h */
