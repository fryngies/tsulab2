/*
    ПОЛИЗ
*/

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cctype>
#include "stack.h"
#include "rpn.h"
#include "tools.h"

const char L_ENTER_STATEMENT[] = "Введите выражение";
const char L_POSTFIX[] = "ПОЛИЗ";
const char L_RESULT[] = "Ответ";

char *getInput(char *str)
{
    int i;
    char ch;

    printf("%s: ", L_ENTER_STATEMENT);
    scanf("%c", &ch);
    for(i = 0; ch != endl(); ++i, scanf("%c", &ch))
        strAppend(str, ch, i);

    return str;
}

int main()
{
    char *postfix = new char[100];
    char *infix = new char[100];
    char *sym = new char[256];

    getInput(infix);
    infix2postfix(infix, postfix, (int)strlen(infix));
    delete[] infix;
    getVars(postfix, sym);

    printf("%s: %s\n", L_POSTFIX, postfix);
    printf("%s: %d\n", L_RESULT, calcPostfix(postfix, sym));

    delete[] postfix;

    return 0;
}
