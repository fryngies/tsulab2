/*
	Выяснить есть ли в графе путь из вершины i в вершину j.
*/

#include <cstdio>
#include <cmath>
#include "bool.cpp"

const char L_MATRIX_RESULT[] = "Матрица";

const char L_ENTER_N_OF_VERTICES[] = "Введите число вершин";
const char L_ENTER_ARCHES[] = "Введите дугу";
const char L_ENTER_FROM[] = "Введите начальную вершину";
const char L_ENTER_TO[] = "Введите конечную вершину";

const char L_PATH_EXISTS[] = "Путь существует";
const char L_PATH_DEXISTS[] = "Путь не существует";

int main()
{
    const int m_l = 4;
    int a = 1, b, i, n, k;
    word m[(int)pow(2, m_l)], mask, v_old, v_new;

    printf("%s 0: ", L_ENTER_ARCHES);
    scanf(" %d %d", &a, &b);
    for(int i = 1; a != 0; ++i)
    {
        printf("%s %d: ", L_ENTER_ARCHES, i);
        m[a-1] |= 1 << (b-1);
        scanf(" %d %d", &a, &b);
    }

    printf("\n");

    printf("%s:\n", L_MATRIX_RESULT);
    for(int i = 0; i < m_l; i++)
    {
        printf("%d: ", i);
        outbool(m[i], m_l);
    }

    printf("\n");

    printf("%s: ", L_ENTER_FROM);
    scanf("%d", &k);

    v_new = m[k-1];
    do
    {
        v_old = v_new;
        for(i = 0, mask = 1; i < m_l; ++i, mask <<= 1)
            if(v_old & mask)
                v_new |= m[i];
    } while (v_new != v_old);

    printf("%s: ", L_ENTER_TO);
    scanf("%d", &k);

    if(v_new & (1 << (k-1)))
        printf("%s.\n", L_PATH_EXISTS);
    else
        printf("%s.\n", L_PATH_DEXISTS);

    return 0;
}
