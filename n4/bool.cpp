#include <cstdio>

typedef unsigned int word;

int weight_bool(word a)
{
    int w = 0;

    for (int i = 0; i < 16; ++i, a >>= 1)
        w += (a & 1);

    return w;
}

void outbool(word v, int n)
{
    word m = 1 << (n-1);
    int i = n;

    while(i > 0)
    {
        if(v & m)
            putchar('1');
        else
            putchar('0');

        m >>= 1;

        --i;
    }

    putchar('\n');
}

word inbool()
{
    char s[33];
    word v = 0, mask = 1;
    gets(s);

    for(int i = 0; i < 33 || s[i] == '\0'; ++i)
    {
        if(s[i] == '1')
            v|=mask;

        mask <<= 1;
    }
    return v;
}

void outmatrix(word v[], int n)
{
    for (int j = 0; j < n; ++j)
        outbool(v[j], n);

    putchar('\n');
}

bool getel(word v, int x, int n)
{
    return v & (1 << (n-x-1));
}
