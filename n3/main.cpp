/*
    Ввести значения двух массивов a и b
    и вывести числа массива a,
    которых нет в массиве b.
*/

#include <cstdio>
#include <cmath>
#include "bool.cpp"

const char L_ENTER_N[] = "n";

int main()
{
    int n, m;
    word v_a = 0, v_b = 0;
    word mask = 1, v;

    printf("%s: ", L_ENTER_N);
    scanf(" %d", &n);

    m = 32;

    printf("\n");
    printf("a:\n");
    for(int i = 0; i < n; i++)
    {
        printf("%d: ", i);
        scanf(" %d", &v);
        v_a |= 1 << v;
    }
    printf("\n");

    printf("b:\n");
    for(int i = 0; i < n; i++)
    {
        printf("%d: ", i);
        scanf(" %d", &v);
        v_b |= 1 << v;
    }
    printf("\n");

    v = (v_a^v_b) & (~v_b);

    for(int i = 0; i < m; i++, mask <<= 1)
        if(mask & v)
            printf("%d ", i);
    printf("\n");

    return 0;
}
