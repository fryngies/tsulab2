#include <cstdio>

struct Node
{
    int k;
    Node *next;
};

Node *Push(Node *p, int k)
{
    Node *t = new Node;
    t->k = k;
    t->next = p;

    return t;
}

Node *Pop(Node *p)
{
    if(!p)
        return 0;

    Node *t = p->next;

    delete p;
    return t;
}

int Top(Node *p)
{
    if(p)
        return p->k;

    return -1;
}

void printStack(Node *p)
{
    Node *l_p = p;

    while(l_p)
    {
        printf("\t%d\n", Top(l_p));

        l_p = l_p->next;
    }
}
