#ifndef stack_h
#define stack_h

struct Stack
{
    int k;
    Stack *next;
};

Stack *MakeEmpty(Stack *p);

Stack *Push(Stack *p, int k);

Stack *Pop(Stack *p);

int PopNum(Stack *p);

int Top(Stack *p);

bool IsEmpty(Stack *p);

int StackWeight(Stack *p);

void PrintStack(Stack *p);

Stack* Add(Stack *p);

Stack* Sub(Stack *p);

Stack* Mul(Stack *p);

Stack* Divide(Stack *p);

#endif /* stack_h */
