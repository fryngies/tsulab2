#!/usr/bin/env python3

import os
import sys
import re
import glob

FILENAME = 'tasklist.txt'
ACCEPTABLE = '/*'
SRC_FILENAME = 'main.cpp'
DESC_ERR = "No desc"

def checkIfAcceptable(cstr, acc):
    return acc in cstr

def checkIfUnacceptable(cstr, unacc):
    return cstr == unacc or cstr == ''

def natural_sort(l):
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]

    return sorted(l, key=alphanum_key)

def getDirList(src_filename=SRC_FILENAME):
    #return = natural_sort([f for f in os.listdir('.') if re.match(r'n?[0-9]', f)])
    l = []

    for f in os.listdir('.'):
        if re.match(r'n?[0-9]', f):
            if os.path.isfile(os.path.join(f, src_filename)):
                l.append(f)

    return natural_sort(l)

def getStart():
    global ACCEPTABLE

    return ACCEPTABLE

def getEnd():
    return getStart()[::-1]

def genLine(line):
    line = line.strip()

    line = ''.join([line, '\n'])
    line = ''.join(['\t', line])

    return line

def readSrc(srcPath, descErr=DESC_ERR, src_filename=SRC_FILENAME):
    try:
        f = open(os.path.join(srcPath, src_filename), 'r')
        line = f.readline()
    except OSError as err:
        return err
    except:
        raise

    if not checkIfAcceptable(line.replace('\n', ''), getStart()):
        return genLine(descErr)

    desc = []
    accR = getEnd()
    while True:
        line = f.readline().strip()

        if line == accR or line == '':
            break

        desc.append(genLine(line))

    f.close()

    return ''.join(desc)

def readSrcs():
    dirList = getDirList()
    descs = []

    for i in range(0, len(dirList)):
        desc = ''.join([dirList[i], ':'])
        desc += readSrc(dirList[i])
        descs.append(desc)

    return descs

def genFile(filename):
    l = readSrcs()

    try:
        f = open(filename, 'w')
    except OSError as err:
        return err
    except:
        raise

    for i in range(0, len(l)):
        try:
            line = f.write(l[i])
            f.write('\n')
        except:
            raise

    f.close()

def main():
    global FILENAME

    genFile(FILENAME)

if __name__ == '__main__':
    main()
