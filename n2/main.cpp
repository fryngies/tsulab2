/*
    Побочная диагональ в строку с наименьшим весом.
*/

#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <cmath>
#include "bool.cpp"

typedef unsigned int word;


int main()
{
    const int m_l = 8;
    word m[m_l];
    word diag = 0;
    int min_w_i = 0, min_w = 0;

    srand(time(NULL));

    for(int i = 0; i < m_l; ++i)
    {
        m[i] = rand() % (int)(pow(2, m_l) - 1);
        printf("%d: ", i + 1);
        outbool(m[i], m_l);
    }

    min_w = weight_bool(m[0], m_l);

    for(int i = 0; i < m_l; ++i)
    {
        diag |= (getel(m[i], m_l-i-1, m_l) << (m_l-i-1));

        if(weight_bool(m[i], m_l) < min_w)
        {
            min_w = weight_bool(m[i], m_l);
            min_w_i = i;
        }
    }

    printf("\n");

    printf("Anti-diagonal: ");
    outbool(diag, m_l);

    printf("Min weight has line %d: ", min_w_i + 1);
    outbool(m[min_w_i], m_l);

    printf("\n");

    m[min_w_i] = diag;
    for(int i = 0; i < m_l; ++i)
    {
        printf("%d: ", i + 1);
        outbool(m[i], m_l);
    }

    return 0;
}
