TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    stack.cpp \
    rpn.cpp \
    tools.cpp

HEADERS += \
    stack.h \
    rpn.h \
    tools.h
