#include <cstdlib>
#include <cstdio>
#include <cstring>
#include "list.h"

struct Node;
struct ListPointers;

ListPointers LPInit(ListPointers p)
{
    p.beg = NULL;
    p.end = NULL;
    
    return p;
}

void ListPrint(ListPointers p)
{
    for(Node *l = p.beg; l; l = l->next)
        printf("\t%s\n", l->str);
}

ListPointers ListPush(ListPointers p, char *str)
{
    Node *n_node = new Node;
    n_node->str = new char[strlen(str) + 1];
    n_node->prev = NULL;

    strcpy(n_node->str, str);
    
    if(!p.beg)
    {
        p.beg = p.end = n_node;
        n_node->next = NULL;
    } else
    {
        p.beg->prev = n_node;
        n_node->next = p.beg;
        p.beg = n_node;
    }

    return p;
}

ListPointers ListPopB(ListPointers p)
{
    if(!p.beg)
        return p;

    Node *q = p.beg;
    
    p.beg->next->prev = 0;
    p.beg = p.beg->next;
    delete[] q->str;
    delete q;
    
    return p;
}

ListPointers ListPopE(ListPointers p)
{
    if(!p.end)
        return p;
    
    Node *q = p.end;
    
    p.end->prev->next = 0;
    p.end = p.end->prev;
    delete[] q->str;
    delete q;
    
    return p;
}

ListPointers FindNDelete(ListPointers p, char *str)
{
    for(Node *l = p.beg->next; l->next; l = l->next)
    {
        if(strcmp(l->str, str))
            continue;

        l->prev->next = l->next;
        l->next->prev = l->prev;
        delete []l->str;
        delete l;
    }
    if(!strcmp(p.beg->str, str))
        ListPopB(p);
    if(!strcmp(p.end->str, str))
        ListPopE(p);

    return p;
}
