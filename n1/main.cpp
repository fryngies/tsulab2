/*
    В булевой матрице обнулить столбцы,
    где нулей больше, чем единиц,
    в остальные записать единицы.
*/

#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <cmath>


typedef unsigned int word;


void outbool(word v, int n)
{
    char s[33];
    word mask = 1;

    for(int i = 0; i < n; ++i)
    {
        if(v & mask)
            s[i] = '1';
        else
            s[i] = '0';

        mask <<= 1;
    }
    s[n] = '\0';

    printf("%s\n", s);
}

void makenull(word v[], int n, int j)
{
    word mask =~ (1 << j);

    for(int i = 0; i < n; ++i)
        v[i] &= mask;
}

void makefull(word v[], int n, int j)
{
    word mask = (1 << j);

    for(int i = 0; i < n; ++i)
        v[i] |= mask;
}


int main()
{
    word m[30], mask = 1;
    int n;

    printf("n: ");
    scanf("%d", &n);
    printf("\n");

    srand(time(NULL));

    for(int i = 0; i < n; ++i)
    {
        m[i] = rand() % (int)(pow(2, n) - 1);
        outbool(m[i], n);
    }

    printf("\n");

    for(int i = 0; i < n; ++i)
    {
        int w = 0;

        for(int j = 0; j < n; ++j)
            if(m[j] & mask)
                ++w;

        if(w >= (n - w))
            makefull(m, n, i);
        else
            makenull(m, n, i);

        mask <<= 1;
    }

    printf("\n");

    for(int i = 0; i < n; ++i)
        outbool(m[i], n);

    return 0;
}
