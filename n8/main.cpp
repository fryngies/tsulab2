/*
    Сформировать список.
    Найти первый и последний нули, удалить узлы между ними.
 */

#include <cstdio>
#include <cstdlib>
#include <ctime>

struct Node {
    int num;
    Node *next;
};

Node *AddToList(Node *lnode, int num)
{
    Node *newel = new Node, *l = lnode;

    newel->num = num;
    newel->next = NULL;

    if(!lnode)
        return newel;

    while(l->next)
        l = l->next;

    l->next = newel;

    return lnode;
}

void dellist(Node *lnode)
{
    Node *p_s = lnode;
    Node *p_tmp = p_s;

    while(p_s)
    {
        p_s = p_s->next;
        delete p_tmp;
        p_tmp = p_s;
    }
}

void printl(Node *l)
{
    for(; l; l = l->next)
        printf("%d ", l->num);
}

int main()
{
    int n;

    int buff_n;

    Node *z1_p = NULL;
    Node *z2_p = NULL;

    Node *lnode_p;
    Node *buff1;
    Node *buff2;

    Node *lnode = NULL;

    printf("n: ");
    scanf("%d", &n);
    for(int i = 0; i < n; ++i)
    {
        printf("%d: ", i+1);
        scanf("%d", &buff_n);
        lnode = AddToList(lnode, buff_n);
    }
    lnode_p = lnode;
    printf("\n");

    printf("%5s" ,"In: ");
    printl(lnode);
    printf("\n");

    while(lnode_p)
    {
        if(lnode_p->num == 0 && z1_p == NULL)
            z1_p = lnode_p;
        else if(lnode_p->num == 0 && z1_p != NULL)
            z2_p = lnode_p;

        lnode_p = lnode_p->next;
    }

    if(z1_p == NULL)
    {
        printf("First zero was not found.\n");
        dellist(lnode);
        return 1;
    }
    if(z2_p == NULL)
    {
        printf("Second zero was not found.\n");
        dellist(lnode);
        return 2;
    }

    buff1 = z1_p->next;
    buff2 = buff1;
    while(buff1 < z2_p)
    {
        buff1 = buff1->next;
        delete buff2;
        buff2 = buff1;
    }
    z1_p->next = z2_p;

    printf("%5s" ,"Out: ");
    printl(lnode);
    printf("\n");

    dellist(lnode);

    return 0;
}
