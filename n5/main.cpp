/*
    Поменять местами строки с минимальными и
    максимальными суммами элементов (суммы находить функциями)
*/

#include <cstdio>
#include <cstdlib>
#include <ctime>


int find_sum(int *a, int n)
{
    int max = 0;

    for(int i = 0; i < n; ++i)
        max += a[i];

    return max;
}

int** rand_matrix(int n, int m, int max)
{
    int **a;

    a = new int*[n];
    for(int i = 0; i < n; ++i)
        a[i] = new int[m];

    for(int i = 0; i < n; ++i)
        for(int j = 0; j < m; ++j)
            a[i][j] = rand() % max;

    return a;
}

void print_matrix(int **a, int n, int m)
{
    for(int i = 0; i < n; ++i)
    {
        for(int j = 0; j < m; ++j)
            printf("%d ", a[i][j]);
        printf("\n");
    }
}

int main()
{
    const int l = 5;
    int **a;
    int min = 1000, max = 0, tmp;
    int min_i, max_i;
    int *p_tmp;

    srand(time(0));
    a = rand_matrix(l, l, 10);

    print_matrix(a, l, l);
    printf("\n");

    for(int i = 0; i < l; ++i)
    {
        tmp = find_sum(a[i], l);

        if(tmp > max)
        {
            max = tmp;
            max_i = i;
        } else if(tmp < min)
        {
            min = tmp;
            min_i = i;
        }
    }

    printf("min_i: %d, max_i: %d\n", min_i+1, max_i+1);
    printf("\n");
    p_tmp = a[min_i];
    a[min_i] = a[max_i];
    a[max_i] = p_tmp;

    print_matrix(a, l, l);

    for(int i = 0; i < l; ++i)
        delete []a[i];
    delete []a;

    return 0;
}
