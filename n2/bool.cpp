#include <cstdio>

typedef unsigned int word;

int weight_bool(word a, int m_l)
{
    int w = 0;

    for(int i = 0; i < m_l; ++i, a >>= 1)
        w += (a & 1);

    return w;
}

void outbool(word v, int n)
{
    word m = 1 << (n-1);
    int i = n;

    while(i > 0)
    {
        if(v & m)
            putchar('1');
        else
            putchar('0');

        putchar(' ');
        m >>= 1;

        --i;
    }

    putchar('\n');
}

void outmatrix(word v[], int n)
{
    for (int j = 0; j < n; ++j)
        outbool(v[j], n);

    putchar('\n');
}

bool getel(word v, int x, int n)
{
    return v & (1 << (n-x-1));
}
