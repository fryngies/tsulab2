/*
    Создать стек с целыми числами
    и разбить его на два с четными
    и нечетными числами, нулевые
    числа удалить.
*/

#include <cstdio>
#include <cstdlib>
#include <ctime>
#include "stack.cpp"

int main()
{
    const int n = 10;
    const int max = 100;

    Node *stack = NULL;
    Node *stack_even = NULL, *stack_odd = NULL;

    srand(time(0));

    printf("Initial stack:\n");
    for(int i = 0; i < n; ++i)
    {
        stack = Push(stack, rand() % max);
        printf("\t%d\n", Top(stack));
    }

    printf("\n");
    printf("------\n");
    printf("\n");

    for(int i = 0; stack; ++i)
    {
        if(Top(stack) % 2 == 0 && Top(stack) > 0)
            stack_even = Push(stack_even, stack->k);
        else if(Top(stack) % 2 != 0)
            stack_odd = Push(stack_odd, stack->k);

        stack = Pop(stack);
    }

    printf("stack_even:\n");
    printStack(stack_even);
    printf("\n");

    printf("stack_odd:\n");
    printStack(stack_odd);
    printf("\n");

    printf("Initial stack:\n");
    printStack(stack);

    return 0;
}
