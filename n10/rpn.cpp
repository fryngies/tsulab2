#include <cstdlib>
#include <cstdio>
#include <cctype>
#include "stack.h"
#include "tools.h"
#include "rpn.h"

const char L_ENTER_VALUE_FOR[] = "Введите значение для";

int getWeight(char ch)
{
    switch (ch)
    {
        case '/':
        case '*':
            return 2;
        case '+':
        case '-':
            return 1;
        default :
            return 0;
    }
}

char *infix2postfix(char *infix, char *postfix, int size)
{
    Stack *s = NULL;
    
    int weight;
    int i = 0;
    int k = 0;
    char ch;

    while (i < size)
    {
        ch = infix[i++];
        
        if(ch == ' ')
            continue;
        
        if(ch == '(')
        {
            s = Push(s, ch);
            continue;
        }
        if(ch == ')')
        {
            while (IsEmpty(s) && Top(s) != '(') {
                postfix[k++] = Top(s);
                s = Pop(s);
            }
            if (IsEmpty(s)) {
                s = Pop(s);
            }
            continue;
        }
        
        weight = getWeight(ch);
        if(weight == 0)
        {
            postfix[k++] = ch;
        }
        else
        {
            if(!IsEmpty(s))
            {
                s = Push(s, ch);
            }
            else
            {
                while (IsEmpty(s) && Top(s) != '(' &&
                       weight <= getWeight(Top(s)))
                {
                    postfix[k++] = Top(s);
                    s = Pop(s);
                    
                }
                s = Push(s, ch);
            }
        }
    }
    while (IsEmpty(s))
    {
        postfix[k++] = Top(s);
        s = Pop(s);
    }
    postfix[k] = ends();
    
    return postfix;
}

char *getVars(char *str, char *sym)
{
    for(int i = 0; i < 255; ++i)
        sym[i] = -1;

    for(int i = 0; str[i] != ends(); ++i)
    {
        if(isalpha(str[i]) && sym[str[i]] == -1)
        {
            fflush(stdin);
            printf("%s %c: ", L_ENTER_VALUE_FOR, str[i]);
            scanf(" %c", &sym[str[i]]);
        }
    }
    
    return sym;
}

int calcPostfix(char *str, char *sym)
{
    Stack *stack = NULL;
    
    for(int i = 0; str[i] != ends(); ++i)
    {
        if(isdigit(str[i]))
        {
            stack = Push(stack, intEq(str[i]));
            continue;
        }
        else if(isalpha(str[i]))
        {
            stack = Push(stack, intEq(sym[str[i]]));
            continue;
        }
        
        switch(str[i])
        {
            case '+':
                stack = Add(stack);
                break;
            case '-':
                stack = Sub(stack);
                break;
            case '*':
                stack = Mul(stack);
                break;
            case '/':
                stack = Divide(stack);
                break;
        }
    }
    
    return Top(stack);
}

