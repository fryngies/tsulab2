#ifndef list_h
#define list_h

#include <stdio.h>

struct Node {
    char *str;
    Node *next;
    Node *prev;
};

struct ListPointers {
    Node *beg;
    Node *end;
};

ListPointers LPInit(ListPointers p);
void ListPrint(ListPointers p);
ListPointers ListPush(ListPointers p, char *str);
ListPointers ListPopB(ListPointers p);
ListPointers ListPopE(ListPointers p);
ListPointers FindNDelete(ListPointers p, char *str);

#endif /* list_h */
