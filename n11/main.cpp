/*
	Создать двунаправленный список с ключом строкой.
	Ввести с клавиатуры строку, если 
	она есть в списке, удалить все её вхождения, 
	иначе добавить её в начало списка.
*/

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include "list.h"
#include "tools.h"

const char L_ENTER_N[] = "Введите количество значений";
const char L_ENTER_STATEMENT[] = "Список";
const char L_TO_DELETE[] = "Ключ";

char *getInput(char *str)
{
    int i;
    char ch;

    fflush(stdin);
    scanf(" %c", &ch);
    for(i = 0; ch != endl(); ++i, scanf("%c", &ch))
        strAppend(str, ch, i);
    
    return str;
}

int main() {
    ListPointers list;
    int l;
    bool stat = false;
    char *str = new char[100];
    
    list = LPInit(list);
    fflush(stdin);
    printf("%s: ", L_ENTER_N);
    scanf("%d", &l);

    for(int i = 0; i < l; ++i)
    {
        printf("%s %d: ", L_ENTER_STATEMENT, i+1);
        getInput(str);
        list = ListPush(list, str);
    }
    
    ListPrint(list);
    
    printf("%s: ", L_TO_DELETE);
    getInput(str);
    
    for(Node *l = list.beg->next; l->next; l = l->next)
    {
        if(strcmp(l->str, str))
            continue;
        
        l->prev->next = l->next;
        l->next->prev = l->prev;
        stat = true;
        delete []l->str;
        delete l;
    }
    if(!strcmp(list.beg->str, str))
    {
        list = ListPopB(list);
        stat = true;
    }
    if(!strcmp(list.end->str, str))
    {
        stat = true;
        list = ListPopE(list);
    }
    if(!stat)
        list = ListPush(list, str);

    ListPrint(list);
    
    return 0;
}
