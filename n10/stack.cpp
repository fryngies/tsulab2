#include <cstdio>
#include "stack.h"
#include "tools.h"

struct Stack;

Stack *MakeEmpty(Stack *p)
{
    if(!p)
        return NULL;
    
    Stack *p_s = p;
    Stack *p_tmp = p_s;
    
    while(p_s)
    {
        p_s = p_s->next;
        delete p_tmp;
        p_tmp = p_s;
    }
    
    return NULL;
}

Stack *Push(Stack *p, int k)
{
    Stack *t = new Stack;
    t->k = k;
    t->next = p;
    
    return t;
}

Stack *Pop(Stack *p)
{
    if(!p)
        return 0;
    
    Stack *t = p->next;
    
    delete p;
    return t;
}

int Top(Stack *p)
{
    if(p)
        return p->k;
    
    return -1;
}

bool IsEmpty(Stack *p)
{
    if(p)
        return 1;
    
    return 0;
}

int StackWeight(Stack *p)
{
    int i;
    
    for(i = 0; p != NULL; ++i)
        p = p->next;
    
    return i;
}

void PrintStack(Stack *p)
{
    Stack *l_p = p;
    
    while(l_p)
    {
        printf("\t%c\n", Top(l_p));
        
        l_p = l_p->next;
    }
}

Stack *Add(Stack *p)
{
    int n1, n2;
    
    if(!p)
        return p;
    n1 = p->k;
    p = Pop(p);
    
    if(!p)
        return p;
    n2 = p->k;
    p = Pop(p);
    
    p = Push(p, n2 + n1);
    
    return p;
}

Stack *Sub(Stack *p)
{
    int n1, n2;
    
    if(!p)
        return p;
    n1 = p->k;
    p = Pop(p);
    
    if(!p)
        return p;
    n2 = p->k;
    p = Pop(p);
    
    p = Push(p, n2 - n1);
    
    return p;
}

Stack *Mul(Stack *p)
{
    int n1, n2;
    
    if(!p)
        return p;
    n1 = p->k;
    p = Pop(p);
    
    if(!p)
        return p;
    n2 = p->k;
    p = Pop(p);
    
    p = Push(p, n2 * n1);
    
    return p;
}

Stack *Divide(Stack *p)
{
    int n1, n2;
    
    if(!p)
        return p;
    n1 = p->k;
    p = Pop(p);
    
    if(!p)
        return p;
    n2 = p->k;
    p = Pop(p);
    
    p = Push(p, n2 / n1);
    
    return p;
}
